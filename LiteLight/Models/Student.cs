﻿using System.Collections.Generic;

namespace LiteLight.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public List<Exam> Exams { get; set; }
    }
}
