﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiteLight.Models
{
    public class MockExamRepository : IExamRepository
    {
        private readonly IKeyRepository _keyRepository = new MockKeyRepository();

        public IEnumerable<Exam> AllExams =>
            new List<Exam>
            {
                new Exam{ ExamId = 0, Responses = new List<int>{  3, 4, 3, 2, 4, 1, 1, 0, 2, 2 }, StudentId = 100, Key = _keyRepository.GetKeyByID(0) },
                new Exam{ ExamId = 0, Responses = new List<int>{  3, -1, 3, 2, -1, 1, 1, 3, 3, 1 }, StudentId = 101, Key = _keyRepository.GetKeyByID(0) },
                new Exam{ ExamId = 0, Responses = new List<int>{  3, 2, 3, 2, 4, 1, 1, 0, 3, 1 }, StudentId = 102, Key = _keyRepository.GetKeyByID(0) },
                new Exam{ ExamId = 0, Responses = new List<int>{  3, 4, 3, 2, 4, -1, 1, 3, 2, 1 }, StudentId = 103, Key = _keyRepository.GetKeyByID(0) },
                new Exam{ ExamId = 1, Responses = new List<int>{ 1,2,3,4,3,2,1,2,0,2,0 }, StudentId = 100, Key = _keyRepository.GetKeyByID(1) },
                new Exam{ ExamId = 1, Responses = new List<int>{ 1,2,3,4,2,2,1,0,0,0,0 }, StudentId = 101, Key = _keyRepository.GetKeyByID(1) },
                new Exam{ ExamId = 1, Responses = new List<int>{ 3,2,3,4,3,2,-1,0,0,0,0 }, StudentId = 102, Key = _keyRepository.GetKeyByID(1) },
                new Exam{ ExamId = 1, Responses = new List<int>{ 1,2,3,4,3,2,1,0,0,0,-1 }, StudentId = 103, Key = _keyRepository.GetKeyByID(1) },
                new Exam{ ExamId = 2, Responses = new List<int>{ 4,4,3,3,3,3,2,2,2,0,0,0,0,1,1,1,1 } , StudentId = 100, Key = _keyRepository.GetKeyByID(2) },
                new Exam{ ExamId = 2, Responses = new List<int>{ 4,4,2,3,3,3,2,2,2,1,0,1,0,1,1,1,1 } , StudentId = 102, Key = _keyRepository.GetKeyByID(2) },
                new Exam{ ExamId = 2, Responses = new List<int>{ 4,3,1,3,2,3,3,2,2,4,4,0,0,1,-1,-1,-1 } , StudentId = 103, Key = _keyRepository.GetKeyByID(2) }
            };

        public IEnumerable<Exam> CurrentExam => throw new NotImplementedException();

        public Exam GetExamByID(int examId)
        {
            return AllExams.FirstOrDefault(p => p.ExamId == examId);
        }

        public IEnumerable<Exam> GetExamsByID(int examId)
        {
            return AllExams.Where(p => p.ExamId == examId);
        }
    }
}
