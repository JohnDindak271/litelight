﻿using System.Collections.Generic;

namespace LiteLight.Models
{
    public class MockStudentRepository : IStudentRepository
    {
        public IEnumerable<Student> AllStudents =>
            new List<Student>
            {
                new Student{StudentId = 100, Name = "Lucy Ball" },
                new Student{ StudentId = 101, Name = "Tweety Bird" },
                new Student{ StudentId = 102, Name = "Dockter Who" },
                new Student{StudentId = 103, Name = "Charles Brown"}
            };
    }
}
