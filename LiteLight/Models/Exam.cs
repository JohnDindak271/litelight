﻿using System.Collections.Generic;

namespace LiteLight.Models
{
    public class Exam
    {
        public Exam()
        {
            Score = 0;
        }
        public int ExamId { get; set; }
        public List<int> Responses { get; set; }
        public int MyProperty { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public Key Key { get; set; }
        public int KeyId { get; set; }
        public double Score { get; set; }
    }
}
