﻿using System.Collections.Generic;

namespace LiteLight.Models
{
    public interface IKeyRepository
    {
        IEnumerable<Key> AllKeys { get;  }
        Key GetKeyByID(int keyId);
    }
}
