﻿using System.Collections.Generic;

namespace LiteLight.Models
{
    public class Key
    {
        public int KeyID { get; set; }
        public List<int> Answers { get; set; }
        public string Title { get; set; }
    }
}
