﻿using System.Collections.Generic;
using System.Linq;

namespace LiteLight.Models
{
    public class MockKeyRepository : IKeyRepository
    {
        public IEnumerable<Key> AllKeys =>
            new List<Key>
            {
                new Key{ KeyID = 0, Answers = new List<int> { 3, 4, 1, 2, 4, 2, 1, 3, 3,1 }, Title = "Math Exam" },
                new Key{ KeyID = 1, Answers = new List<int> { 1,2,3,4,3,2,1,0,0,0,0 }, Title = "English Exam" },
                new Key{ KeyID = 2, Answers = new List<int> { 4,4,4,3,3,3,2,2,4,0,0,0,0,2,1,1,1 }, Title = "Biology Exam" }
            };

        public Key GetKeyByID(int keyId)
        {
            return AllKeys.FirstOrDefault(p => p.KeyID == keyId);
        }
    }
}
