﻿using System.Collections.Generic;

namespace LiteLight.Models
{
    public interface IExamRepository
    {
        IEnumerable<Exam> AllExams { get; }
        IEnumerable<Exam> CurrentExam { get; }
        Exam GetExamByID(int testId);
        IEnumerable<Exam> GetExamsByID(int testId);
    }
}
