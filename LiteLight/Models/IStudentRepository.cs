﻿using System.Collections.Generic;

namespace LiteLight.Models
{
    public interface IStudentRepository
    {
        IEnumerable<Student> AllStudents { get; }
    }
}
