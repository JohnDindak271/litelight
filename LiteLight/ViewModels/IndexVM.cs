﻿using LiteLight.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace LiteLight.ViewModels
{
    public class IndexVM
    {
        internal void PopDrops()
        {
            KeyItems = new List<SelectListItem>();
            foreach(Key k in Keys)
                KeyItems.Add(new SelectListItem()
                {
                    Value = k.KeyID.ToString(),
                    Text = k.Title
                });

       
            StudentItems = new List<SelectListItem>();
            foreach (Student s in Students)
                StudentItems.Add(new SelectListItem()
                {
                    Value = s.StudentId.ToString(),
                    Text = s.Name
                });            
        }
        public IEnumerable<Key> Keys { get; set; }


        public IEnumerable<Student> Students { get; set; }

        public List<SelectListItem> KeyItems { get; set; }

    
        public List<SelectListItem> StudentItems { get; set; }
        public int SelectedStudent { get; set; }

        public int SelectedKey { get; set; }

        public IEnumerable<Exam> Exams {get; set; }
    }
}
