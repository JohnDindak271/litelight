﻿using LiteLight.Models;
using System.Collections.Generic;

namespace LiteLight.ViewModels
{
    public class StudentExamsVM
    {
        public StudentExamsVM(int sid)
        {
            SID = sid;
            Average = 0;
        }

        public int SID { get; set; }
        public IEnumerable<Exam> Exams { get; set; }
        public Student Student{ get; set; }
        public IEnumerable<Key> MissedExams { get; set; }
        public double Average { get; set; }
    }
}
