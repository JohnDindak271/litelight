﻿using LiteLight.Models;
using System.Collections.Generic;

namespace LiteLight.ViewModels
{
    public class StudentListVM
    {
        public IEnumerable<Student> Students { get; set; }
        public string ExamName { get; set; }
    }
}
