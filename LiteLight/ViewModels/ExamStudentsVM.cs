﻿using LiteLight.Models;
using System.Collections.Generic;

namespace LiteLight.ViewModels
{
    public class ExamStudentsVM
    {
        public ExamStudentsVM(int selectedKey)
        {
            ExamID = selectedKey;
        }

        public int ExamID { get; set; }
        public IEnumerable<Exam> Exams { get; set; }
        public IEnumerable<Student> Students { get; set; }
        public List<int> MissedAllAnswers { get; set; }
    }
}
