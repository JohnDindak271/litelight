﻿using LiteLight.Models;
using LiteLight.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace LiteLight.Controllers
{
    public class HomeController : Controller
    {
        private readonly IExamRepository _examRepository;
        private readonly IKeyRepository _keyRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IExamRepository examRepository, IKeyRepository keyRepository, IStudentRepository studentRepository)
        {
            _logger = logger;
            _examRepository = examRepository;
            _keyRepository = keyRepository;
            _studentRepository = studentRepository;
        }

        public IActionResult Index()
        {
            IndexVM v = new IndexVM();
            v.Keys = _keyRepository.AllKeys;
            v.Students = _studentRepository.AllStudents;
            v.Exams = _examRepository.AllExams;
            v.PopDrops();
            return View(v);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
