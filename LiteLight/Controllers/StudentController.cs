﻿using LiteLight.Models;
using LiteLight.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace LiteLight.Controllers
{
    public class StudentController : Controller
    {
        private readonly IExamRepository _examRepository;
        private readonly IKeyRepository _keyRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly ILogger<StudentController> _logger;

        public StudentController(ILogger<StudentController> logger, IExamRepository examRepository, IKeyRepository keyRepository, IStudentRepository studentRepository)
        {
            _logger = logger;
            _examRepository = examRepository;
            _keyRepository = keyRepository;
            _studentRepository = studentRepository;
        }

        public IActionResult ExamStudents(IndexVM v)
        {
            var vm = new ExamStudentsVM(v.SelectedKey);
            var list = new List<int>();
            vm.Exams = _examRepository.GetExamsByID(v.SelectedKey);
            vm.Students = _studentRepository.AllStudents;
            vm.MissedAllAnswers = new List<int>();

            foreach (var exam in vm.Exams)
            {
                exam.Score = exam.Key.Answers.Count();

                for (int i = 0; i < exam.Key.Answers.Count(); i++)
                {
                    if (exam.Key.Answers[i] != exam.Responses[i])
                    {
                        exam.Score--;
                        list.Add(i);
                    }   
                }

                exam.Score = Math.Round((exam.Score/exam.Key.Answers.Count())*100, 2);
            }

            var missedAnswers = list.GroupBy(x => x)
                               .Select(g => new {Value = g.Key, Count = g.Count()})
                               .Where(f => f.Count == vm.Exams.Count());
            
            foreach (var missed in missedAnswers)
            {
                vm.MissedAllAnswers.Add(missed.Value);
            }
            
            return View(vm);
        }

        public IActionResult StudentExams(int sid)
        {
            var v = new StudentExamsVM(sid);
            var exams = _keyRepository.AllKeys;
            var missedExam = new List<Key>();
            var examCount = 0;
            v.Exams = _examRepository.AllExams.Where(x => x.StudentId == sid);
            v.Student = _studentRepository.AllStudents.FirstOrDefault(x => x.StudentId == sid);

            foreach(var exam in v.Exams)
            {
                exam.Score = exam.Key.Answers.Count();
                
                for (int i = 0; i < exam.Key.Answers.Count(); i++)
                {
                    if (exam.Key.Answers[i] != exam.Responses[i])
                    {
                        exam.Score--;
                    }   
                }

                exam.Score = Math.Round((exam.Score/exam.Key.Answers.Count())*100, 2);
                v.Average += exam.Score;
                examCount++;
            }

            foreach (var exam in exams)
            {
                if (v.Exams.Any(x => x.ExamId == exam.KeyID) != true)
                {
                    missedExam.Add(exam);
                    examCount++;
                }
            }

            v.MissedExams = missedExam;
            v.Average = Math.Round(v.Average/examCount, 2);

            return PartialView(v);
        }
    }
}
